    -- START
WHENEVER SQLERROR EXIT sql.sqlcode;

SET AUTOCOMMIT ON;

SET SERVEROUTPUT ON;

-- check keys

EXECUTE check_unique('select 1 from a_group_plan group by group_code, l_plan_id having count(1) > 1');

EXECUTE check_unique('select 1 from a_group_plan_policy_type group by group_code, l_plan_id having count(1) > 1');

EXECUTE check_unique('select 1 from a_group_policy_type_prod_opt group by group_code, l_plan_id having count(1) > 1');

--

EXECUTE exec_immediate('alter table a_group_plan add l_groups_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan add l_group_plan_status_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_plan_policy_type add l_billing_cycle_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_policy_type add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_plan_policy_type add l_group_plan_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_policy_type add l_base_product_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_policy_type add l_customer_account_id number(10)');

EXECUTE exec_immediate('alter table a_group_policy_type_prod_opt add l_product_id number(10)');

EXECUTE exec_immediate('alter table a_group_policy_type_prod_opt add l_product_option_id number(10)');

EXECUTE exec_immediate('alter table a_group_policy_type_prod_opt add l_group_plan_policy_type_id number(10)');

EXECUTE exec_immediate('alter table a_group_policy_type_prod_opt add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table group_plan add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_plan add l_plan_id varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_policy_type add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_policy_type add l_plan_id varchar2(50)');

EXECUTE exec_immediate('alter table group_policy_type_prod_option add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_policy_type_prod_option add l_plan_id varchar2(50)');

-- start: insert into group plan

MERGE INTO a_group_plan a USING ( SELECT
    l_group_code,
    l_plan_id
                                  FROM
    group_plan
)
gp ON (
    a.group_code = gp.l_group_code
    AND a.l_plan_id = gp.l_plan_id
)
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

MERGE INTO a_group_plan a USING ( SELECT
    id,
    l_group_code
                                  FROM
    groups
)
g ON (
    a.group_code = g.l_group_code
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_groups_id = g.id;

-- l_group_plan_status_id
-- set GP status same with GPPT status (1-1)

MERGE INTO a_group_plan a USING ( SELECT
    gppt.group_code,
    gppt.l_plan_id,
    gppt.l_pocy_eff_date,
    gps.id status_id
                                  FROM
    a_group_plan_policy_type gppt
    INNER JOIN group_plan_status gps ON gps.code = gppt.grp_plan_pol_type_status_code
)
g ON (
    a.group_code = g.group_code
    AND a.l_plan_id = g.l_plan_id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_group_plan_status_id = g.status_id;

INSERT INTO group_plan (
    id,
    groups_id,
    group_plan_status_id,
    industry_code,
    name,
    creuser,
    credate,
    upduser,
    upddate,
    billing_contact_code,
    l_group_code,
    l_plan_id
)
    SELECT
        group_plan_s.NEXTVAL id,
        l_groups_id AS groups_id,
        l_group_plan_status_id AS group_plan_status_id,
        industry_code,
        name,
        creuser,
        crt_date credate,
        upduser,
        upd_date upddate,
        sub_agent_code AS billing_contact_code,
        group_code AS l_group_code,
        l_plan_id
    FROM
        a_group_plan
    WHERE
        l_record_exists = 'N'
        AND   l_groups_id IS NOT NULL;
-- end: insert into group plan

-- start: insert into group plan policy type
MERGE INTO a_group_plan_policy_type a USING ( SELECT
    id,
    product_id
                                              FROM
    product_option
)
po ON (
    a.l_plan_id = po.id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_base_product_id = po.product_id;

-- group_plan_id

MERGE INTO a_group_plan_policy_type a USING ( SELECT
    id,
    l_group_code,
    l_plan_id
                                              FROM
    group_plan
)
gp ON (
    a.group_code = gp.l_group_code
    AND a.l_plan_id = gp.l_plan_id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_group_plan_id = gp.id;

-- l_customer_account_id for BILL_TO_POLICYHOLDER_FLAG = 'Y'

MERGE INTO a_group_plan_policy_type a USING ( SELECT
    id,
    l_group_code
                                              FROM
    customer_account
)
ca ON (
    a.group_code = ca.l_group_code
    AND a.bill_to_policyholder_flag = 'Y'
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_customer_account_id = ca.id;

-- l_customer_account_id for BILL_TO_POLICYHOLDER_FLAG = 'N'

MERGE INTO a_group_plan_policy_type a USING ( SELECT
    ca.id customer_account_id,
    a.advisor_code
                                              FROM
    advisor a
    INNER JOIN customer_account ca ON ca.id = a.customer_account_id
)
ca ON (
    a.bill_to_advisor_code = ca.advisor_code
    AND a.bill_to_policyholder_flag = 'N'
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_customer_account_id = ca.customer_account_id;

-- start: insert into billing cycle

MERGE INTO a_group_plan_policy_type a USING ( SELECT
    l_group_code,
    l_plan_id
                                              FROM
    group_plan_policy_type
)
gppt ON (
    a.group_code = gppt.l_group_code
    AND a.l_plan_id = gppt.l_plan_id
)
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

UPDATE a_group_plan_policy_type
    SET
        l_billing_cycle_id = billing_cycle_s.NEXTVAL
WHERE
    l_base_product_id IS NOT NULL
    AND   l_record_exists = 'N';

INSERT INTO billing_cycle (
    id,
    receipt_method_code,
    frequency_code,
    start_date,
    billed_to_date,
    billing_raise_days,
    upduser
)
    SELECT
        agppt.l_billing_cycle_id AS id,
        ca.receipt_method_code,
        g.frequency_code,
--        agppt.l_pocy_eff_date start_date,
        g.start_date,
        agppt.l_pocy_eff_date billed_to_date,
        30 AS billing_raise_days,
        agppt.upduser
    FROM
        a_group_plan_policy_type agppt
        INNER JOIN groups g ON agppt.group_code = g.l_group_code
        INNER JOIN customer_account ca ON ca.id = agppt.l_customer_account_id
    WHERE
        agppt.l_base_product_id IS NOT NULL
        AND   agppt.l_billing_cycle_id IS NOT NULL
        AND   agppt.l_record_exists = 'N';
-- end: insert into billing cycle

INSERT INTO group_plan_policy_type (
    id,
    group_plan_id,
    grp_plan_pol_type_status_code,
    renewal_period_code,
    profit_share_basis_code,
    employee_prem_type_code,
    partner_family_prem_type_code,
    underwriter_id,
    policy_type_id,
    contract_period_code,
    unsub_payment_source_code,
    base_product_id,
    customer_account_id,
    initial_enroll_cutoff_days,
    renewal_lead_days,
    renewal_cutoff_days,
    employ_start_cutoff_days,
    send_accept_cert_flag,
    billing_cycle_id,
    in_renewal_flag,
    send_employee_flag,
    send_group_flag,
    active_flag,
    creuser,
    credate,
    upduser,
    upddate,
    arrears_letter_set_id,
    start_date,
    send_arrears_let,
    claim_recover_arrears,
    maternity_wait_prd_waived,
    medical_wait_prd_waived,
    qualifying_wait_prd_waived,
    philhealth_inclusive,
    premium_waived,
    l_group_code,
    l_plan_id
)
    SELECT
        group_plan_policy_type_s.NEXTVAL AS id,
        l_group_plan_id AS group_plan_id,
        grp_plan_pol_type_status_code,
        renewal_period_code,
        profit_share_basis_code,
        nvl(employee_prem_type_code, 'AGED') as employee_prem_type_code,
        nvl(partner_family_prem_type_code, 'AGED') as partner_family_prem_type_code,
        underwriter_id,
        policy_type_id,
        contract_period_code,
        unsub_payment_source_code,
        l_base_product_id AS base_product_id,
        l_customer_account_id AS customer_account_id,
        initial_enroll_cutoff_days,
        renewal_lead_days,
        renewal_cutoff_days,
        employ_start_cutoff_days,
        send_accept_cert_flag,
        l_billing_cycle_id AS billing_cycle_id,
        in_renewal_flag,
        send_employee_flag,
        send_group_flag,
        active_flag,
        creuser,
        crt_date credate,
        upduser,
        upd_date upddate,
        arrears_letter_set_id,
        l_pocy_eff_date start_date,
        send_arrears_let,
        claim_recover_arrears,
        maternity_wait_prd_waived,
        medical_wait_prd_waived,
        qualifying_wait_prd_waived,
        philhealth_inclusive,
        last_premium_waived premium_waived,
        group_code AS l_group_code,
        l_plan_id
    FROM
        a_group_plan_policy_type
    WHERE
        l_base_product_id IS NOT NULL
        AND   l_group_plan_id IS NOT NULL
        AND   l_record_exists = 'N';
-- end: insert into group plan policy type

-- start: insert into group policy type prod option

MERGE INTO a_group_policy_type_prod_opt a USING ( SELECT
    l_group_code,
    l_plan_id
                                                  FROM
    group_policy_type_prod_option
)
gptpo ON (
    a.group_code = gptpo.l_group_code
    AND a.l_plan_id = gptpo.l_plan_id
)
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

-- TODO: work around on product_option to be used (used latest PRODUCT_OPTION)
-- map l_product_id and l_product_option_id

-- l_product_option_id
update a_group_policy_type_prod_opt set l_product_option_id=l_plan_id;

-- l_product_id
merge into a_group_policy_type_prod_opt a
using (
  select product_id, id product_option_id from product_option
) p
on (a.l_product_option_id = p.product_option_id)
when matched then update set
a.l_product_id = p.product_id;

-- map gppt.id using group_code and l_plan_id for BASE

MERGE INTO a_group_policy_type_prod_opt a USING ( SELECT
    id,
    l_group_code,
    l_plan_id
                                                  FROM
    group_plan_policy_type
)
gppt ON (
    a.group_code = gppt.l_group_code
    AND a.l_plan_id = gppt.l_plan_id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_group_plan_policy_type_id = gppt.id;

-- load gptpo base

INSERT INTO group_policy_type_prod_option (
    id,
    product_id,
    product_option_id,
    subsidy_basis_code,
    subsidy_level_code,
    group_plan_policy_type_id,
    active_flag,
    creuser,
    credate,
    upduser,
    upddate,
    l_group_code,
    l_plan_id
)
    SELECT
        group_policy_type_prod_opt_s.NEXTVAL,
        l_product_id,
        l_product_option_id,
        subsidy_basis_code,
        subsidy_level_code,
        l_group_plan_policy_type_id,
        active_flag,
        creuser,
        crt_date credate,
        upduser,
        upd_date upddate,
        group_code AS l_group_code,
        l_plan_id
    FROM
        a_group_policy_type_prod_opt
    WHERE
        l_group_plan_policy_type_id IS NOT NULL
        AND   l_product_id IS NOT NULL
        AND   l_product_option_id IS NOT NULL
        AND   l_record_exists = 'N';

-- load gptpo riders (cross multiply with all GP)

EXECUTE exec_immediate('create index agptpo_idx1 on a_group_policy_type_prod_opt(group_code, product_option_code)');

INSERT INTO group_policy_type_prod_option (
    id,
    product_id,
    product_option_id,
    subsidy_basis_code,
    subsidy_level_code,
    group_plan_policy_type_id,
    active_flag,
    creuser,
    credate,
    upduser,
    upddate,
    l_group_code,
    l_plan_id
)
    SELECT
        group_policy_type_prod_opt_s.NEXTVAL,
        a2.l_product_id,
        a2.l_product_option_id,
        a2.subsidy_basis_code,
        a2.subsidy_level_code,
        gppt.id,
        a2.active_flag,
        a2.creuser,
        a2.crt_date credate,
        a2.upduser,
        a2.upd_date upddate,
        a2.group_code AS l_group_code,
        a2.l_plan_id
    FROM
        (
            SELECT
                group_code,
                product_option_code
            FROM
                a_group_policy_type_prod_opt
            WHERE
                is_rider = 'Y'
                AND   l_record_exists = 'N'
            GROUP BY
                group_code,
                product_option_code
        ) agptpo
        INNER JOIN group_plan_policy_type gppt ON gppt.l_group_code = agptpo.group_code
        INNER JOIN a_group_policy_type_prod_opt a2 ON a2.group_code = agptpo.group_code
                                                      AND a2.product_option_code = agptpo.product_option_code
                                                      AND a2.l_product_id IS NOT NULL
                                                      AND a2.l_product_option_id IS NOT NULL;
-- end: insert into group policy type prod option

-- start: insert into group plan product ratecard

INSERT INTO group_plan_product_ratecard (
    id,
    admin_ratecard_code,
    underwriter_ratecard_code,
    discriminator_code,
    group_policy_type_prod_opt_id,
    start_date,
    end_date,
    creuser,
    credate,
    upduser,
    upddate
)
    SELECT
        group_plan_product_ratecard_s.NEXTVAL id,
        por.admin_ratecard_code,
        por.underwriter_ratecard_code,
        'NORMAL' discriminator_code,
        gptpo.id group_policy_type_prod_opt_id,
        gppt.start_date,
        NULL AS end_date,
        gppt.creuser,
        gppt.credate,
        gppt.upduser,
        gppt.upddate
    FROM
        group_policy_type_prod_option gptpo
        INNER JOIN product_option_ratecard por ON gptpo.product_option_id = por.product_option_id
        INNER JOIN group_plan_policy_type gppt ON gppt.id = gptpo.group_plan_policy_type_id
    WHERE
        NOT EXISTS (
            SELECT
                1
            FROM
                group_plan_product_ratecard gppr
            WHERE
                gppr.group_policy_type_prod_opt_id = gptpo.id
        );
-- end: insert into group plan product ratecard

-- reports

UPDATE a_group_plan a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_plan gp
        WHERE
            a.group_code = gp.l_group_code
            AND   a.l_plan_id = gp.l_plan_id
    );

EXECUTE migration_stats('a_group_plan');

UPDATE a_group_plan_policy_type a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_plan_policy_type gppt
        WHERE
            a.group_code = gppt.l_group_code
            AND   a.l_plan_id = gppt.l_plan_id
    );

EXECUTE migration_stats('a_group_plan_policy_type');

UPDATE a_group_policy_type_prod_opt a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_policy_type_prod_option gptpo
        WHERE
            a.group_code = gptpo.l_group_code
            AND   a.l_plan_id = gptpo.l_plan_id
    );

EXECUTE migration_stats('a_group_policy_type_prod_opt');

COMMIT;

quit;

-- END
