-- START
WHENEVER SQLERROR EXIT ROLLBACK;

SET AUTOCOMMIT ON;

SET SERVEROUTPUT ON;

-- check keys

EXECUTE check_unique('select 1 from a_groups group by code having count(1) > 1');

EXECUTE check_unique('select 1 from a_group_address group by group_code||address_type_code having count(1) > 1');

EXECUTE check_unique('select 1 from a_group_contact group by l_pocy_oid having count(1) > 1');

EXECUTE check_unique('select 1 from a_group_customer_account group by group_code having count(1) > 1');

--

EXECUTE exec_immediate('alter table a_groups add l_record_exists char(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_address add region_id number(10)');

EXECUTE exec_immediate('alter table a_group_address add city varchar2(250 byte)');

EXECUTE exec_immediate('alter table a_group_address add postal_address_id number(10)');

EXECUTE exec_immediate('alter table a_group_address add physical_address_id number(10)');

EXECUTE exec_immediate('alter table a_group_address add l_groups_id number(10)');

EXECUTE exec_immediate('alter table a_group_address add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_contact add l_person_id number(10)');

EXECUTE exec_immediate('alter table a_group_contact add l_groups_id number(10)');

EXECUTE exec_immediate('alter table a_group_contact add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_customer_account add l_record_exists varchar2(1) default ''N''');

EXECUTE exec_immediate('alter table a_group_customer_account add l_groups_id number(10)');

EXECUTE exec_immediate('alter table a_group_customer_account add l_person_id number(10)');

EXECUTE exec_immediate('alter table a_group_customer_account add l_address_id number(10)');

EXECUTE exec_immediate('alter table groups add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table groups add l_pocy_oid varchar2(50)');

EXECUTE exec_immediate('alter table group_address add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_contact add l_pocy_oid varchar2(50)');

EXECUTE exec_immediate('alter table customer_account add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table person modify first_name varchar2(100)');

-- start: insert groups

MERGE INTO a_groups a USING ( SELECT
    id,
    l_group_code
                              FROM
    groups
)
g ON ( a.code = g.l_group_code )
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

-- add BACOLOD to sales_region_id

MERGE INTO sales_region a USING dual ON ( a.code = 'BACOLOD' )
WHEN NOT MATCHED THEN INSERT (
    id,
                                code,
                            name,
                        sort_order,
                    creuser,
                upduser,
            active_flag,
        receipt_prefix,
    short_code
) VALUES (
    1006,
                                'BACOLOD',
                            'Bacolod',
                        99,
                    2,
                2,
            'N',
        'B',
    'BC'
);

INSERT INTO groups (
    id,
    group_type_code,
    group_status_code,
    code,
    name,
    sales_region_id,
    creuser,
    credate,
    upduser,
    upddate,
    administrator_id,
    start_date,
    end_date,
    renewal_type_code,
    membership_fee,
    policy_holder,
    business_type,
    tax_code,
    tax_payment_type_code,
    reinstate_date,
    frequency_code,
    underwriter_id,
    policy_type_id,
    termination_type_code,
    reinstatement_type_code,
    cwt,
    waive_surcharge,
    group_code_activated,
    cancellation_date,
    travel_type_code,
    renewal_count,
    percent_refund_flag,
    percent_refund,
    renewal_tag,
    RENEWAL_TAG_TYPE_CODE,
    group_identification_type,
    group_identification_number,
    l_group_code,
    l_pocy_oid,
    inception_date
)
    SELECT
        groups_s.NEXTVAL AS id,
        group_type_code,
        DECODE(group_status_code,'OPEN','PENDING',group_status_code) AS group_status_code,
        code,
        name,
        sales_region_id,
        creuser,
        crt_date,
        upduser,
        upd_date,
        administrator_id,
        first_eff_date start_date,
        DECODE(group_status_code,'INFORCE',NULL,end_date) end_date,
        renewal_type_code,
        'N' as membership_fee,
        policyholder policy_holder,
        business_type,
        tax_code,
        tax_payment_mode tax_payment_type_code,
        reinstate_date,
        frequency_code,
        underwriter_id,
        policy_type_id,
        termination_type_code,
        reinstatement_type_code,
        cwt,
        'N' as waive_surcharge,
        group_code_activated,
        cancellation_date,
        travel_type_code,
        renewal_count,
        'N' as percent_refund_flag,
        0 as percent_refund,
        'Y' as renewal_tag,
        'IN_RENEWAL' as RENEWAL_TAG_TYPE_CODE,
        group_identification_type,
        group_identification_number,
        code AS l_group_code,
        l_pocy_oid,
        inception_date
    FROM
        a_groups
    WHERE
        l_record_exists = 'N';
-- end: insert groups

-- start: insert group address

-- l_record_exists

MERGE INTO a_group_address a USING ( SELECT
    l_group_code, address_type_code
                                     FROM
    group_address
)
ga ON ( a.group_code = ga.l_group_code AND a.address_type_code = ga.address_type_code )
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

-- region_id, city

UPDATE a_group_address a
    SET
        region_id = (
            SELECT
                MAX(c.region_id)
            FROM
                postal_code pc
                INNER JOIN city c ON c.id = pc.city_id
            WHERE
                pc.code = a.postcode
        ),
        city = (
            SELECT
                MAX(c.name)
            FROM
                postal_code pc
                INNER JOIN city c ON c.id = pc.city_id
            WHERE
                pc.code = a.postcode
        )
WHERE
    a.postcode IS NOT NULL
    AND   a.l_record_exists = 'N';

-- l_groups_id

MERGE INTO a_group_address a USING ( SELECT
    id,
    l_group_code
                                     FROM
    groups
)
g ON (
    a.group_code = g.l_group_code
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_groups_id = g.id;

INSERT INTO address (
    id,
    groups_id,
    addr_1,
    addr_2,
    addr_3,
    city,
    postcode,
    region_id,
    country_id,
    creuser,
    credate,
    upduser,
    upddate
)
    SELECT 
        address_s.NEXTVAL,
         l_groups_id,
        addr_1,
        addr_2,
        addr_3,
        city,
        postcode,
        region_id,
        country_id,
        creuser,
        crtdate,
        upduser,
        upddate
    FROM (
        SELECT DISTINCT
            l_groups_id,
            addr_1,
            addr_2,
            addr_3,
            city,
            postcode,
            region_id,
            country_id,
            creuser,
            crt_date crtdate,
            upduser,
            upd_date upddate
        FROM
            a_group_address
        WHERE
            l_groups_id IS NOT NULL
            AND   l_record_exists = 'N'
    )
    ;

-- physical_address_id

MERGE INTO a_group_address a USING ( SELECT
    id,
    groups_id
                                     FROM
    address
                                     WHERE
    groups_id IS NOT NULL
)
ad ON (
    a.l_groups_id = ad.groups_id
    AND a.address_type_code = 'PHYSICAL'
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.physical_address_id = ad.id;

-- postal_address_id

MERGE INTO a_group_address a USING ( SELECT
    id,
    groups_id
                                     FROM
    address
                                     WHERE
    groups_id IS NOT NULL
)
ad ON (
    a.l_groups_id = ad.groups_id
    AND a.address_type_code = 'POSTAL'
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.postal_address_id = ad.id;

INSERT INTO group_address (
    id,
    address_type_code,
    groups_id,
    address_id,
    creuser,
    credate,
    upduser,
    upddate,
    l_group_code
)
    SELECT
        group_address_s.NEXTVAL,
        address_type_code,
        l_groups_id,
        physical_address_id address_id,
        creuser,
        crt_date cre_date,
        upduser,
        upd_date upddate,
        group_code AS l_group_code
    FROM
        a_group_address
    WHERE
        address_type_code = 'PHYSICAL'
        AND   l_groups_id IS NOT NULL
        AND   l_record_exists = 'N';

INSERT INTO group_address (
    id,
    address_type_code,
    groups_id,
    address_id,
    creuser,
    credate,
    upduser,
    upddate,
    l_group_code
)
    SELECT
        group_address_s.NEXTVAL,
        address_type_code,
        l_groups_id,
        postal_address_id address_id,
        creuser,
        crt_date cre_date,
        upduser,
        upd_date upddate,
        group_code AS l_group_code
    FROM
        a_group_address
    WHERE
        address_type_code = 'POSTAL'
        AND   l_groups_id IS NOT NULL
        AND   l_record_exists = 'N';
-- end: insert group address

-- start: insert into group_contact

MERGE INTO a_group_contact a USING ( SELECT
    *
                                     FROM
    (
        SELECT
            l_pocy_oid,
            ROW_NUMBER() OVER(
                PARTITION BY l_pocy_oid
                ORDER BY
                    1
            ) drank
        FROM
            group_contact
    )
                                     WHERE
    drank = 1
)
gc ON ( a.l_pocy_oid = gc.l_pocy_oid )
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

UPDATE a_group_contact
    SET
        surname = TRIM(surname),
        first_name = TRIM(first_name),
        office_no = DECODE(office_no,'0',NULL,office_no),
        mobile_no = DECODE(mobile_no,'0',NULL,mobile_no),
        fax_no = DECODE(fax_no,'0',NULL,fax_no),
        l_person_id = person_s.NEXTVAL
WHERE
    l_record_exists = 'N';

INSERT INTO person (
    id,
    surname,
    first_name,
    phone_work,
    phone_mobile,
    phone_fax,
    email,
    email_confirm_flag,
    active_flag,
    creuser,
    credate,
    upduser,
    upddate
)
    SELECT
        l_person_id AS id,
        surname,
        first_name,
        office_no AS phone_work,
        mobile_no AS phone_mobile,
        fax_no AS phone_fax,
        email,
        email_confirm_flag,
        active_flag,
        creuser,
        crt_date AS credate,
        upduser,
        upd_date AS upddate
    FROM
        a_group_contact
    WHERE
        l_record_exists = 'N';

MERGE INTO a_group_contact a USING ( SELECT
    id,
    l_pocy_oid
                                     FROM
    groups
)
g ON (
    a.l_pocy_oid = g.l_pocy_oid
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_groups_id = g.id;

INSERT INTO group_contact (
    id,
    contact_type_code,
    groups_id,
    person_id,
    creuser,
    credate,
    upduser,
    upddate,
    l_pocy_oid
)
    SELECT
        group_contact_s.NEXTVAL AS id,
        'KEY' AS contact_type_code,
        l_groups_id AS groups_id,
        l_person_id AS person_id,
        creuser,
        crt_date AS credate,
        upduser,
        upd_date AS upddate,
        l_pocy_oid
    FROM
        a_group_contact
    WHERE
        l_groups_id IS NOT NULL
        AND   l_record_exists = 'N';

INSERT INTO group_contact (
    id,
    contact_type_code,
    groups_id,
    person_id,
    creuser,
    credate,
    upduser,
    upddate,
    l_pocy_oid
)
    SELECT
        group_contact_s.NEXTVAL AS id,
        'AUTHORISER' AS contact_type_code,
        l_groups_id AS groups_id,
        l_person_id AS person_id,
        creuser,
        crt_date AS credate,
        upduser,
        upd_date AS upddate,
        l_pocy_oid
    FROM
        a_group_contact
    WHERE
        l_groups_id IS NOT NULL
        AND   l_record_exists = 'N';
-- end: insert into group_contact

-- start: insert into group customer account

MERGE INTO a_group_customer_account a USING ( SELECT
    l_group_code
                                              FROM
    customer_account
)
ca ON ( a.group_code = ca.l_group_code )
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

-- l_groups_id

MERGE INTO a_group_customer_account a USING ( SELECT
    id,
    l_group_code
                                              FROM
    groups
)
g ON (
    a.group_code = g.l_group_code
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_groups_id = g.id;

-- l_person_id

MERGE INTO a_group_customer_account a USING ( SELECT
    groups_id,
    person_id
                                              FROM
    group_contact
                                              WHERE
    contact_type_code = 'KEY'
)
gc ON (
    a.l_groups_id = gc.groups_id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_person_id = gc.person_id;

-- l_address_id

EXECUTE exec_immediate('create index agca_idx01 on a_group_customer_account(group_code)');

EXECUTE exec_immediate('create index ga_idx01 on group_address(l_group_code)');

MERGE INTO a_group_customer_account a USING ( SELECT
    address_id,
    l_group_code
                                              FROM
    group_address
                                              WHERE
    address_type_code = 'PHYSICAL'
)
ga ON (
    a.group_code = ga.l_group_code
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_address_id = ga.address_id;

EXECUTE exec_immediate('drop index ga_idx01');

INSERT INTO customer_account (
    id,
    customer_account_type_code,
    group_billing_contact_id,
    address_id,
    groups_id,
    bill_to_name,
    send_invoice_flag,
    email_invoice_flag,
    email,
    dd_authorised_flag,
    active_flag,
    creuser,
    credate,
    upduser,
    upddate,
    invoice_document_set_code,
    receipt_method_code,
    tax_code,
    l_group_code
)
    SELECT
        customer_account_s.NEXTVAL id,
        'GROUP' AS customer_account_type_code,
        agca.l_person_id AS group_billing_contact_id,
        agca.l_address_id AS address_id,
        agca.l_groups_id AS groups_id,
        agca.bill_to_name,
        agca.send_invoice_flag,
        agca.email_invoice_flag,
        agca.email,
        agca.dd_authorised_flag,
        agca.active_flag,
        agca.creuser,
        agca.crt_date AS credate,
        agca.upduser,
        agca.upd_date AS upddate,
        agca.invoice_document_set_code,
        agca.receipt_method_code,
        agca.tax_code,
        group_code
    FROM
        a_group_customer_account agca
    WHERE
        agca.l_record_exists = 'N'
        AND   l_address_id IS NOT NULL
        AND   agca.bill_to_advisor_flag = 'N';

-- reports

UPDATE a_groups a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            groups g
        WHERE
            a.code = g.l_group_code
    );

EXECUTE migration_stats('a_groups');

UPDATE a_group_address a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_address ga
        WHERE
            a.group_code = ga.l_group_code
    );

EXECUTE migration_stats('a_group_address');

UPDATE a_group_contact a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_contact gc
        WHERE
            a.l_pocy_oid = gc.l_pocy_oid
    );

EXECUTE migration_stats('a_group_contact');

UPDATE a_group_customer_account a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            customer_account ca
        WHERE
            a.group_code = ca.l_group_code
    );

EXECUTE migration_stats('a_group_customer_account');

COMMIT;

quit;

---- END