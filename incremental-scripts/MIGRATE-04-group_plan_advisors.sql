WHENEVER SQLERROR EXIT sql.sqlcode;

SET AUTOCOMMIT ON;

SET SERVEROUTPUT ON;

-- check keys

EXECUTE check_unique('select 1 from a_group_plan_advisors group by group_code, l_plan_id, advisor_code having count(1) > 1');

--

EXECUTE exec_immediate('alter table a_group_plan_advisors add l_group_plan_advisors_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_advisors add l_group_plan_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_advisors add l_advisor_id number(10)');

EXECUTE exec_immediate('alter table a_group_plan_advisors add l_withholding_tax_rate number(14,6)');

EXECUTE exec_immediate('alter table a_group_plan_advisors add l_record_exists varchar2(1 byte) default ''N''');

EXECUTE exec_immediate('alter table group_plan_advisors add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_advisors add l_plan_id varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_advisors add l_advisor_code varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_commission add l_group_code varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_commission add l_plan_id varchar2(50)');

EXECUTE exec_immediate('alter table group_plan_commission add l_advisor_code varchar2(50)');

-- incremental

MERGE INTO a_group_plan_advisors a USING (
 SELECT
    l_group_code,
    l_plan_id,
    l_advisor_code
                                           FROM
    group_plan_advisors
)
gadv ON (
    a.group_code = gadv.l_group_code
    AND a.l_plan_id = gadv.l_plan_id
    AND a.advisor_code = gadv.l_advisor_code
)
WHEN MATCHED THEN UPDATE SET a.l_record_exists = 'Y';

-- group_plan_advisor_id

UPDATE a_group_plan_advisors
    SET
        l_group_plan_advisors_id = group_plan_advisors_s.NEXTVAL
WHERE
    l_record_exists = 'N';

-- advisor_id

MERGE INTO a_group_plan_advisors a USING ( SELECT
    id,
    advisor_code,
    withholding_tax_rate
                                           FROM
    advisor
)
s ON (
    a.advisor_code = s.advisor_code
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_advisor_id = s.id,
a.l_withholding_tax_rate = s.withholding_tax_rate;

-- group_plan_id

MERGE INTO a_group_plan_advisors a USING ( SELECT
    id,
    l_group_code,
    l_plan_id
                                           FROM
    group_plan
)
gp ON (
    a.group_code = gp.l_group_code
    AND a.l_plan_id = gp.l_plan_id
    AND a.l_record_exists = 'N'
)
WHEN MATCHED THEN UPDATE SET a.l_group_plan_id = gp.id;

-- insert into group_plan_advisors

INSERT INTO group_plan_advisors (
    id,
    group_plan_id,
    advisor_id,
    creuser,
    credate,
    upduser,
    upddate,
    primary_flag,
    commission_rate,
    start_date,
    end_date,
    secondary_flag,
    withholding_tax_rate,
    l_group_code,
    l_plan_id,
    l_advisor_code
)
    SELECT
        l_group_plan_advisors_id,
        l_group_plan_id,
        l_advisor_id,
        creuser,
        crt_date,
        upduser,
        upd_date upddate,
        primary_flag,
        commission_rate,
        start_date,
        end_date,
        nvl(secondary_flag,'N') secondary_flag,
        l_withholding_tax_rate AS withholding_tax_rate,
        group_code AS l_group_code,
        l_plan_id,
        advisor_code AS l_advisor_code
    FROM
        a_group_plan_advisors
    WHERE
        l_advisor_id IS NOT NULL
        AND   l_group_plan_id IS NOT NULL
        AND   l_record_exists = 'N';

-- insert into group_plan_commission

INSERT INTO group_plan_commission (
    id,
    group_plan_advisors_id,
    group_policy_type_prod_opt_id,
    commission_rate,
    creuser,
    credate,
    upduser,
    upddate,
    l_group_code,
    l_plan_id,
    l_advisor_code
)
SELECT
    group_plan_commission_s.nextval as id,
    l_group_plan_advisors_id as group_plan_advisors_id,
    null as group_policy_type_prod_opt_id,
    commission_rate,
    creuser,
    crt_date as credate,
    upduser,
    upd_date as upddate,
    group_code as l_group_code,
    l_plan_id,
    advisor_code as l_advisor_code
FROM
    a_group_plan_advisors
WHERE
    l_advisor_id IS NOT NULL
    AND l_group_plan_id IS NOT NULL
    AND l_record_exists = 'N';

-- reports

UPDATE a_group_plan_advisors a
    SET
        l_record_exists = 'S'
WHERE
    NOT EXISTS (
        SELECT
            1
        FROM
            group_plan_advisors gadv
        WHERE
            a.group_code = gadv.l_group_code
            AND   a.l_plan_id = gadv.l_plan_id
            AND   a.advisor_code = gadv.l_advisor_code
    );

EXECUTE migration_stats('a_group_plan_advisors');

COMMIT;

quit;
